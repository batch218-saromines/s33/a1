// console.log('checking');


const url = 'https://jsonplaceholder.typicode.com'

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch(`${url}/todos`)
.then(res => res.json())
.then(json => console.log(json.map(list => list.title)));




// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


fetch(`${url}/todos/1`)
.then(res => res.json())
.then(object => console.log(`The item "${object.title}" on the list has a status of false`))



// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.


fetch(`${url}/todos`,
{
	method: 'POST',
	headers: 
	{
		'Content-type' : 'application/json'
	},
	body: JSON.stringify(
	{
		title: 'Created To Do List Item',
		completed: false,
		userId: 1
	})
})
.then((response)=>response.json())
.then((json)=> console.log(json));





// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID



fetch(`${url}/todos/1`, {
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
	"userId": 1,
    "title": "Updated To Do List Item",
    "description" : "To update the my to do list with a different data structure.",
    "status": "Completed",
    "dateCompleted" : "Pending"
	})
})
.then(response => response.json())
.then(object => console.log(object))




// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.


fetch(`${url}/todos/1`, 
{
	method: 'PATCH',
	headers: 
	{
		'Content-type' : 'application/json'
	},
	body: JSON.stringify
	({
    	"status": "Complete",
    	"dateCompleted": "07/09/21"
	})
})
.then(res => res.json())
.then(object => console.log(object))




// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch(`${url}/todos/3`, 
{
	method: "DELETE",
})
.then((response)=>response.json())
.then((json)=> console.log(json));
